# IMGvehicle API

imgvehicle.com is an automotive search engine for diagrams, wiring harness etc. 

Database contains over than 30000 different car diagrams like fuse box diagrams, timing gear diagrams and much more. All [car diagrams](https://imgvehicle.com) are sorted by make, model and year. With advanced search you can find exactly a diagram which you're looking for. In our DB we have brands such as:

- [Audi](https://imgvehicle.com/audi)
- [BMW](https://imgvehicle.com/bmw)
- [Chevrolet](https://imgvehicle.com/chevrolet)
- [Chrysler](https://imgvehicle.com/chrysler)
- [Citroen](https://imgvehicle.com/citroen)
- [Dodge](https://imgvehicle.com/dodge)
- [Ford](https://imgvehicle.com/ford)
- [GMC](https://imgvehicle.com/gmc)
- [Honda](https://imgvehicle.com/honda)
- [Hyundai](https://imgvehicle.com/hyundai)
- [Mercedes](https://imgvehicle.com/mercedes-benz)
- [Saab](https://imgvehicle.com/saab)
- [Subaru](https://imgvehicle.com/subaru)
- [Toyota](https://imgvehicle.com/toyota)
- [Volkswagen](https://imgvehicle.com/volkswagen)
- [Volvo](https://imgvehicle.com/volvo)

and more

We have created an API to give you possibility to display diagrams on your website. You can use diagrams as you want - for DIY websites, personal purposes or for store/garage websites.

To use API you need to have API key. Simply ping me or contact me andreas@imgvehicle.com to get one. Personal usage only! Commerce usage with free plan is forbidden. Feel free to contribute in this project!


## Endpoints

#### Categories

api/categories

#### Models

api/[MODEL]

#### Diagram categories

api/[MODEL]/categories

#### Diagrams

api/diagram/[ID]

## Update: 10.01.2021

We have added guides and articles available through API. There are several categories of articles: [care](https://imgvehicle.com/category/care), [electrical](https://imgvehicle.com/category/electrical), [general](https://imgvehicle.com/category/general), [guides](https://imgvehicle.com/category/guides), [maintenance](https://imgvehicle.com/category/maintenance) and [specifications](https://imgvehicle.com/category/specifications). To access this resources you need to have PRO plan. Some articles are related with diagrams. 

Sample output:
```
article: {
    title: 'My car',
    description: 'Description of the car',
    tags: ['car', 'driving'],
    diagrams: [37254, 81625, 39264, 39273]
}
```